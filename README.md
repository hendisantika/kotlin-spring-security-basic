# Kotlin Spring Security Basic

#### Run locally

```
git clone https://gitlab.com/hendisantika/kotlin-spring-security-basic.git
```

```
gradle clean bootRun --info
```

#### Username & Password

Use this username & password : 
```
naruto\naruto
```

#### Screen shot

Login page

![Login page](img/login.png "Login Page")

Success page

![Success Page](img/success.png "Success Page")
